package sbu.cs.exception;

public class UnrecognizedCommand extends ApException{
    public UnrecognizedCommand(String message)
    {
        super(message);
    }
}
