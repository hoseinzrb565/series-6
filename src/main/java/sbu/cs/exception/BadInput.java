package sbu.cs.exception;

public class BadInput extends ApException{
    public BadInput(String message)
    {
        super(message);
    }
}
