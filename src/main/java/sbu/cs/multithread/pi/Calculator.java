package sbu.cs.multithread.pi;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;

public class Calculator implements Runnable {

    private final int counter;
    private final MathContext mathContext;


    public Calculator(int counter) {
        this.counter = counter;
        this.mathContext = new MathContext(10000, RoundingMode.HALF_DOWN);
    }

    @Override
    public void run() {
        Pi.endComputation(compute());
    }

    private BigDecimal compute() {
        final long k = 8 * counter;
        BigDecimal currentValue = new BigDecimal(0, mathContext);

        BigDecimal term1 = new BigDecimal(4).divide(new BigDecimal(k + 1), mathContext);
        BigDecimal term2 = new BigDecimal(-2).divide(new BigDecimal(k + 4), mathContext);
        BigDecimal term3 = new BigDecimal(-1).divide(new BigDecimal(k + 5), mathContext);
        BigDecimal term4 = new BigDecimal(-1).divide(new BigDecimal(k + 6), mathContext);

        return currentValue.add(term1).add(term2).add(term3).add(term4).
        divide(new BigDecimal(16).pow(counter));
    }
}
