package sbu.cs.multithread.pi;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class PICalculator {

    private final ExecutorService executorService = Executors.newFixedThreadPool(4);

    /**
     * calculate pi and represent it as string with given floating point number (numbers after .)
     * check test cases for more info
     * check pi with 1000 digits after floating point at https://mathshistory.st-andrews.ac.uk/HistTopics/1000_places/
     *
     * @param floatingPoint number of digits after floating point
     * @return pi in string format
     */
    public String calculate(int floatingPoint) throws InterruptedException {
        for (int i = 0; i < 1000; i++)
        {
            try
            {
                executorService.submit(new Calculator(i));
            }
            catch(Exception e)
            {
                e.printStackTrace();
            }
        }
        executorService.shutdown();
        executorService.awaitTermination(10000, TimeUnit.MILLISECONDS);

        return Pi.getPi().toString().substring(0, floatingPoint + 2);

    }
}
