package sbu.cs.multithread.pi;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;

public class Pi {

    private static BigDecimal pi = new BigDecimal(0,
            new MathContext(10000, RoundingMode.HALF_DOWN));

    synchronized public static void endComputation(BigDecimal bd) {
        pi = pi.add(bd);
    }

    public static BigDecimal getPi() {
        return pi;
    }

}
